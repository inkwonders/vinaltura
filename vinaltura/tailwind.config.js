module.exports = {
    purge: [
        './resources/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/**/*.js',
        './resources/**/*.vue',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                red: {
                    vinaltura: '#ff3039'
                },
                beige: {
                    vinaltura: '#e9e6da',
                    select: '#d6bdac',
                    fondo: '#f1f1f1',
                    texto: '#c7a792',
                },
                brown: {
                    vinaltura: '#503629'
                },
                mate: '#2d2d2d'
            },
            backgroundImage: {
                inicio: "url('/assets/img/fondo_inicio.png')"
            },
            height: {
                pixel: '1px',
                imagen: 'calc(100vh - 15rem)',
            },
            width: {
                pixel: '1px'
            },
            borderRadius: {
                circle: '50%'
            },
            inset: {
                custom: 'calc(100% - 0.875rem)'
            },
        },
    },
    variants: {
        extend: {
            fontWeight: ['hover', 'focus'],
        },
    },
    plugins: [],
}