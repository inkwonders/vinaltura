<!DOCTYPE html>
<html lang="es" class="w-full min-h-screen">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @yield('meta')

    <title>@yield('title')</title>

    <link href="/css/app.css" rel="stylesheet">
    
    @yield('css')
</head>
<body class="w-full min-h-screen">

    <div id="app" class="relative w-full min-h-screen overflow-x-hidden overscroll-y-auto">
        <header class="fixed top-0 left-0 right-0 z-10 flex flex-col justify-between w-full h-20 bg-white">
            <div class="w-full h-14 px-4 py-2 mt-0.5 flex flex-row justify-between items-center text-brown-vinaltura">
                <div class="flex items-center justify-end h-full w-9">
                    <div class="relative flex flex-col py-2 cursor-pointer w-9 h-9 justify-evenly">
                        <div class="w-full h-pixel bg-brown-vinaltura"></div>
                        <div class="w-full h-pixel bg-brown-vinaltura"></div>
                    </div>
                </div>
                <div class="flex items-center justify-center w-2/3 h-full p-1">
                    <a href="/tienda" class="block h-full">
                        <img class="h-full max-w-full max-h-full cursor-pointer" src="/assets/img/svg/logo_head.svg" alt="Vinaltura">
                    </a>
                </div>
                <div class="flex items-center justify-end h-full w-9">
                    <tienda-icon
                        v-model="cantidad_carrito"
                    />
                </div>
            </div>
            <div class="flex items-center justify-center w-full h-5 text-xs font-bold text-white border-b border-red-600 bg-red-vinaltura">
                <span>ENVÍO GRATIS A PARTIR DE 3 BOTELLAS</span>
            </div>
        </header>
    
        @yield('container')

        <footer class="w-full h-40 p-4 bg-beige-vinaltura text-brown-vinaltura">
            <div class="flex flex-col items-center w-full h-full text-base text-center justify-evenly">
                <span>VINALTURA 2020 <div class="inline text-xs">®</div></span>
                <span class="cursor-pointer hover:underline">AVISO DE PRIVACIDAD</span>
                <span>DESARROLLADO POR INKWONDERS <div class="inline text-xs">®</div></span>
            </div>
        </footer>
    </div>

    @yield('js')

    <script src="/js/app.js"></script>

</body>
</html>
