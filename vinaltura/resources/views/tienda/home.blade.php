@extends('layouts.base')

@section('meta')
@endsection

@section('title')
    Vinaltura | Tienda
@endsection

@section('css')
@endsection

@section('container')
    <section class="w-full h-screen pt-20 text-white">  {{-- Aquí es necesario tener el padding --}}
        <div class="flex flex-col justify-between h-full bg-center bg-cover p-7 bg-inicio">
            <div class="flex justify-start w-full">
                <div class="text-2xl font-bold text-left uppercase w-28">
                    <span>Únete al Wine Club</span>
                </div>
            </div>
            <div class="flex items-end justify-between w-full">
                <div class="w-11 h-1 bg-white my-1.5"></div>
                <div class="text-2xl font-bold text-right uppercase w-36">
                    <span>Obtén Grandes Beneficios</span>
                </div>
            </div>
        </div>
    </section>
    <section class="w-full p-4">
        <secciones-vinos
            v-model="cantidad_carrito"
        ></secciones-vinos>

        {{-- <div class="w-full h-10"></div>

        <vinos-premiados
            v-model="cantidad_carrito"
        ></vinos-premiados> --}}
    </section>
    <div class="w-full h-8"></div>
    <section class="w-full">
        <div class="flex items-center justify-center w-full h-20 text-2xl font-bold text-center text-brown-vinaltura">
            <span>WINE CLUB</span>
        </div>
        <div class="w-full h-auto p-4">
            <div class="flex justify-start w-full bg-center bg-cover h-imagen bg-inicio">
                <div class="w-1/2 p-6">
                    <img src="/assets/img/svg/vinaltura_wineclub.svg" alt="Vinaltura">
                </div>
            </div>
            <div class="w-full pt-1">
                <div class="w-full p-5 text-2xl font-light leading-6 bg-mate text-beige-texto">
                    <span>ÚNETE A NUESTRO WINE CLUB, OBTEN 15% DE DESCUENTO EN TODAS TUS COMPRAS, ADEMÁS DE OTROS BENEFICIOS.</span>
                </div>
            </div>
            <div class="w-full pt-1">
                <div class="w-full p-2 font-bold text-center text-white cursor-pointer bg-red-vinaltura">
                    <span>QUIERO UNIRME</span>
                </div>
            </div>
        </div>
    </section>
    <div class="w-full h-16"></div>
@endsection

@section('js')
@endsection
