@extends('layouts.base')

@section('meta')
@endsection

@section('title')
    Vinaltura | Carrito
@endsection

@section('css')
@endsection

@section('container')
    <section class="w-full h-screen pt-20">  {{-- Aquí es necesario tener el padding --}}
        <carrito
            v-model="cantidad_carrito"
        ></carrito>
    </section>
    <div class="w-full h-16"></div>
@endsection

@section('js')
@endsection
