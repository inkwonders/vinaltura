// require('./bootstrap');

window.Vue = require('vue').default;


Vue.component('vino-tipo', require('./components/VinoTipo.vue').default);
Vue.component('tienda-icon', require('./components/TiendaIcon.vue').default);
Vue.component('secciones-vinos', require('./components/SeccionesVinos.vue').default);
Vue.component('carrito', require('./components/Carrito.vue').default);


Vue.directive('click-outside', {
    bind: function(el, binding, vnode) {
        this.event = function(event) {
            if (!(el == event.target || el.contains(event.target))) {
                vnode.context[binding.expression](event);
            }
        };
        document.body.addEventListener('click', this.event)
    },
    unbind: function(el) {
        document.body.removeEventListener('click', this.event)
    }
});

const app = new Vue({
    el: '#app',
    data: {
        cantidad_carrito: []
    },
    beforeCreate: function() {
        let categorias = [{
                nombre: 'BAJÍO',
                subcategorias: [{
                        nombre: 'Tinto Bajío',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: '/assets/img/malbec.png'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: '/assets/img/malbec.png'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: '/assets/img/malbec.png'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: '/assets/img/malbec.png'
                            }
                        ]
                    },
                    {
                        nombre: 'Espuma Rosé',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Blanco',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Rosé Bajío',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                ]
            },
            {
                nombre: 'TERRUÑOS',
                subcategorias: [{
                        nombre: 'Tinto Bajío',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Espuma Rosé',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Blanco',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Rosé Bajío',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                ]
            },
            {
                nombre: 'VARIETALES',
                subcategorias: [{
                        nombre: 'Tinto Bajío',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Espuma Rosé',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Blanco',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Rosé Bajío',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                ]
            },
            {
                nombre: 'ENSAYOS',
                subcategorias: [{
                        nombre: 'Tinto Bajío',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Espuma Rosé',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Blanco',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    },
                    {
                        nombre: 'Rosé Bajío',
                        vinos_premiados: [{
                            id: 3,
                            titulo: 'Malbec',
                            anio: '2018',
                            descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                            precio: '445',
                            wine_club: '400',
                            maximo: 'no',
                            url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                        }],
                        vinos: [{
                                id: 1,
                                titulo: 'Sauvignon Blanc',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 2,
                                titulo: 'Riesling',
                                anio: '2019',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            },
                            {
                                id: 3,
                                titulo: 'Malbec',
                                anio: '2018',
                                descripcion: 'La malbec es una variedad de uva morada usada para hacer vino tinto. Las uvas tienden a tener un color oscuro y abundantes taninos, y se hicieron conocidas por ser una de seis uvas permitidas en las mezclas del vino de Burdeos.',
                                precio: '445',
                                wine_club: '400',
                                maximo: 'no',
                                url_imagen: 'https://misvinos.com/assets/imgs/vinopremiado1.jpg'
                            }
                        ]
                    }
                ]
            }
        ]

        let informacion_categoria = [
            { titulo: 'FICHA TÉCNICA' },
            { titulo: 'NOTA DE CATA' },
            { titulo: 'MARIDAJE' }
        ]

        localStorage.setItem('categorias', JSON.stringify(categorias))
        localStorage.setItem('informacion_categoria', JSON.stringify(informacion_categoria))
    },
    mounted() {}
});